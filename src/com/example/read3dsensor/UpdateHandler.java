package com.example.read3dsensor;

import java.util.Date;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.widget.ArrayAdapter;

public class UpdateHandler extends Handler {
	private boolean keep_going = false;
	private ArrayAdapter<String> adapter;
	
	public UpdateHandler(ArrayAdapter<String> a){
		adapter = a;
	}
	
	@Override
    public void handleMessage(Message msg) {
		//Check if we are supposed to keep going or not
		if (msg.what == -1)
		{
			keep_going = false;
		}
		else if (msg.what == 287 && keep_going == false)
		{
			keep_going = true;
			//Call yourself again in a bit
			Message tmp_message = new Message();
			tmp_message.what = 1;
			sendMessageDelayed(tmp_message, 250);
		}
		else if(msg.what == 1)
		{
			if (keep_going){
				//Update the GL scene based on the sensor's orientation
				CustomResponse res;
				float[] orient;
				int[] timestamp;
				try {
					res = Sensor.getInstance().getCorrectedSensorData();
					orient = res.getmCords();
					timestamp = res.getmTimeStamp();
					String[] txt = new String[orient.length];
					for(int i = 0;i < orient.length; i++){
						txt[i] = Float.toString(orient[i]);
					}
					String[] txt1 = new String[timestamp.length];
					for(int i = 0;i < timestamp.length; i++){
						txt1[i] = Integer.toString(Integer.parseInt(Integer.toHexString(timestamp[i]),16));
					}
					
					long time = Long.parseLong(TextUtils.join("", txt1));
					Date dt = new Date((long)time * 1000);
					String t = "Timestamp : " + dt.toString();
					
					adapter.add(t + " -- Cords: " + TextUtils.join(", ", txt));
					//Call yourself again in a bit
					sendMessageDelayed(obtainMessage(1,0,0), 1000);
				} catch (Exception e) {
					return;
				}

			}
		}
    }

}
