package com.example.read3dsensor;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity {

	private BluetoothAdapter mBTAdapter;
	private ListView mCordList;
	private ArrayAdapter<String> mListAdapter;
	private Sensor sensor;
	private UpdateHandler mHandler;
	
	private boolean isReading = false;
	
	private static final int ENABLE_BT_REQ = 1; 
	public static final String TAG = "Sensor Reader";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mBTAdapter = BluetoothAdapter.getDefaultAdapter(); 

		if(mBTAdapter == null){
			Toast.makeText(this, "Bluetooth not detected.", Toast.LENGTH_SHORT).show();
			finish();
			return;
		}

		mCordList = (ListView) findViewById(R.id.cords);
		mListAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
		mCordList.setAdapter(mListAdapter);
		
		if(!mBTAdapter.isEnabled()){
			startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), ENABLE_BT_REQ);
		}else{
			Log.d(TAG, "On create : init()");
			init();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	private void init(){
		if(sensor == null){
			try{
				sensor = Sensor.getInstance();
				Log.d(TAG, "init() : getting sensor instance");
			}catch(Exception e){}

			if(sensor == null){
				Toast.makeText(getApplicationContext(), "Unable to connect to sensor", Toast.LENGTH_SHORT).show();
				finish();
				return;
			}else{
				Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_SHORT).show();
				Log.d(TAG, "init() : Enabling header response");
				sensor.enableTimestampInHeader();
			}
			
			if(!isReading){
				mHandler = new UpdateHandler(mListAdapter);
			    Message start_again_message = new Message();
				start_again_message.what = 287;
			    mHandler.sendMessage(start_again_message);
			    isReading = true;
			}			
		}
	}
	
	@Override
	protected void onStop() {
		if(isReading){
		    Message stopMessage = new Message();
		    stopMessage.what = -1;
		    mHandler.sendMessage(stopMessage);
		    isReading = false;			
		}
		super.onStop();
	}
	
	@Override
	protected void onDestroy() {
    	if(isReading)
    	{
	    	Message quit_message = new Message();
	    	quit_message.what = -1;
	    	mHandler.sendMessage(quit_message);
	    	isReading = false;
    	}
    	try {
			Sensor.getInstance().close();
		} catch (Exception e) {
			e.printStackTrace();
		}
    	super.onDestroy();
    	int pid = android.os.Process.myPid();
    	android.os.Process.killProcess(pid);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == ENABLE_BT_REQ){
			if(resultCode == RESULT_OK){
				Toast.makeText(getApplicationContext(), "Bluetooth enabled", Toast.LENGTH_SHORT).show();
				init();
			}else{
				Toast.makeText(getApplicationContext(), "Unable to enable Bluetooth.", Toast.LENGTH_SHORT).show();
				finish();
			}
		}
	}

}
