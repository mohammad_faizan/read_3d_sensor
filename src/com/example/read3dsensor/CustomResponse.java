package com.example.read3dsensor;

public class CustomResponse {

	private int[] mTimeStamp;
	private float[] mCords;

	public int[] getmTimeStamp() {
		return mTimeStamp;
	}
	public void setmTimeStamp(int[] mTimeStamp) {
		this.mTimeStamp = mTimeStamp;
	}
	public float[] getmCords() {
		return mCords;
	}
	public void setmCords(float[] mCords) {
		this.mCords = mCords;
	}
}
